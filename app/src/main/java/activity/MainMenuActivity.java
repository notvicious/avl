package activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import pac.avl.R;

/**
 * Класс активити "Главного меню".
 */
public class MainMenuActivity extends AppCompatActivity {

    /**
     * Метод, который вызывается при создании активити.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
    }

    /**
     * Метод обработки события клик по кнопке "Добавить значение".
     * @param view - компонент на который кликнули.
     */
    public void onClickAddMain(View view) {
        Intent intent = new Intent(this, AddActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Метод обработки события клик по кнопке "Удалить значение".
     * @param view - компонент на который кликнули.
     */
    public void onClickRemoveMain(View view) {
        Intent intent = new Intent(this, RemoveActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Метод обработки события клик по кнопке "Посмотреть дерево".
     * @param view - компонент на который кликнули.
     */
    public void onClickSeeMain(View view) {
        Intent intent = new Intent(this, TreeActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Метод обработки события клик по кнопке "Выход".
     * @param view - компонент на который кликнули.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void onClickExit(View view) {
        finishAndRemoveTask();
    }
}
