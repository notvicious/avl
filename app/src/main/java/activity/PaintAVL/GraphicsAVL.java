package activity.PaintAVL;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import model.AVLSet;

/**
 * Класс элемента для отображения АВЛ-дерева.
 */
public class GraphicsAVL extends View {
    /**
     *Расстояние по оси OY между вершинами дерева.
     */
    static final int H = 100;
    /**
     * Расстояние по оси OX между вершина дерева.
     */
    static final int W = 100;
    /**
     * Радиус вершины.
     */
    static final int R = 50;
    /**
     * Размер текста.
     */
    static final int TEXT_SIZE = 50;
    /**
     * Размер линии, которая соединяет вершины.
     */
    static final int LINE_SIZE = 6;
    /**
     * Объект, в котором хранятся настройка рисования вершины.
     */
    final Paint paintCircle;
    /**
     * Объект, в котором хранятся настройка рисования значения вершины.
     */
    final Paint paintText;
    /**
     * Объект, в котором хранятся настройка рисования линий между вершинами.
     */
    final Paint paintLine;
    /**
     * Дерево, которое уже расположено на плоскости и его нужно только нарисовать.
     */
    final TreePosition treePosition;

    /**
     * Конструктор.
     * @param context - контекст элемента.
     * @param avl - АВЛ-дерево, которое нужно нарисовать.
     */
    public GraphicsAVL(Context context, AVLSet<Integer> avl) {
        super(context);
        treePosition = new TreePosition(avl);

        paintCircle = new Paint();
        paintCircle.setColor(getResources().getColor(pac.avl.R.color.colorNode));

        paintText = new Paint();
        paintText.setColor(getResources().getColor(pac.avl.R.color.colorTextOnTree));
        paintText.setTextSize(TEXT_SIZE);

        paintLine = new Paint();
        paintLine.setColor(getResources().getColor(pac.avl.R.color.colorLineBetweenNode));
        paintLine.setStrokeWidth(LINE_SIZE);
    }

    /**
     * Метод отрисовки элемента.
     * @param canvas - холст, на котором нужно нарисовать дерево.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        drawTree(canvas, treePosition.getRoot());
    }

    /**
     * Рекурсивная функция, которая обходит дерево и рисует его.
     * @param canvas - холст, на котором нужно нарисовать дерево.
     * @param nd - текущая вершина.
     */
    void drawTree(Canvas canvas, Node nd) {
        if (nd == null) return;
        final float x = calcX(nd);
        final float y = calcY(nd);

        if (nd.left != null) canvas.drawLine(x, y, calcX(nd.left), calcY(nd.left), paintLine);
        if (nd.right != null) canvas.drawLine(x, y, calcX(nd.right), calcY(nd.right), paintLine);
        canvas.drawCircle(x, y, R, paintCircle);
        canvas.drawText(nd.value.toString(), x - R / 2, y, paintText);

        drawTree(canvas, nd.left);
        drawTree(canvas, nd.right);
    }

    /**
     * Вычисление координаты вершины по оси OX.
     * @param nd - вершина.
     * @return координата вершины по оси OX.
     */
    float calcX(Node nd) {
        return nd.x * W;
    }

    /**
     * Вычисление координаты вершины по оси OY.
     * @param nd - вершина.
     * @return координата вершины по оси OY.
     */
    float calcY(Node nd) {
        return (nd.y + 1)* H;
    }
}
