package activity.PaintAVL;

import model.AVLSet;

/**
 * Класс АВЛ-дерева на плоскости.
 */
public class TreePosition {
    /**
     * Корень дерева.
     */
    final Node root;
    /**
     * Текущая координата по оси OX.
     */
    float xpos;

    /**
     * Конструктор.
     * @param avl - АВЛ-дерево, которой нужно расположить на плоскости.
     */
    public TreePosition(AVLSet avl) {
        xpos = 1;
        root = construction(avl, new Float(1), avl.getValueRoot());
    }

    /**
     * Получение корня дерева.
     * @return корень дерева.
     */
    public Node getRoot() { return root; }

    /**
     * Рекурсивная функция обхода и расположения вершин АВЛ-дерева на плоскости.
     * @param avl - АВЛ-дерево, которое нужно расположить на плоскости.
     * @param y - текущая координата по оси OY.
     * @param value - значение текущей вершины.
     * @return вершина, которая расположена на координатной плоскости.
     */
    Node construction(AVLSet avl, float y, Object value) {
        if (value == null) return null;
        Object leftValue = avl.getValueLeft(value);
        Object rightValue = avl.getValueRight(value);

        Node leftNode = construction(avl, y + 1, leftValue);

        float x = xpos++;

        Node rightNode = construction(avl, y + 1, rightValue);

        if (leftNode != null && rightNode != null) x = (leftNode.x + rightNode.x) / 2;
        return new Node(value, x, y, leftNode, rightNode);
    }
}
