package activity.PaintAVL;

/**
 * Класс вершины дерева на плоскости.
 */
public class Node {
    /**
     * Значение вершины.
     */
    final Object value;
    /**
     * Координата по оси OX.
     */
    final float x;
    /**
     * Координата по оси OY.
     */
    final float y;
    /**
     * Левый сын.
     */
    final Node left;
    /**
     * Правый сын.
     */
    final Node right;

    /**
     * Конструктор.
     * @param value - значение вершины.
     * @param x - координата по оси OX.
     * @param y - координата по оси OY.
     * @param left - левый сын.
     * @param right - правый сын.
     */
    public Node(Object value, float x, float y, Node left, Node right) {
        this.value = value;
        this.x = x;
        this.y = y;
        this.left = left;
        this.right = right;
    }
}