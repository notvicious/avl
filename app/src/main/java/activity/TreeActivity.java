package activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import activity.PaintAVL.GraphicsAVL;
import model.AVLSet;
import pac.avl.R;

/**
 * Класс активити "Просмотр дерева на плоскости".
 */
public class TreeActivity extends AppCompatActivity {
    public static AVLSet<Integer> avl = new AVLSet<>();

    /**
     * Метод, который вызывается при создании активити.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tree);

        LinearLayout layout = (LinearLayout) findViewById(R.id.lin_layout);
        GraphicsAVL graphicsAVL = new GraphicsAVL(this, avl);
        layout.addView(graphicsAVL, 1);
    }

    /**
     * Метод обработки события клик по кнопке "Назад".
     * @param view - компонент на который кликнули.
     */
    public void onClickBackTree(View view) {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
        finish();
    }
}
