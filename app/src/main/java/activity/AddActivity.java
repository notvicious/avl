package activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import pac.avl.R;

/**
 * Класс активити "Добавить значение"
 */
public class AddActivity extends AppCompatActivity {

    /**
     * Метод, который вызывается при создании активити.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
    }

    /**
     * Метод обработки события клик по кнопке "Назад".
     * @param view - компонент на который кликнули.
     */
    public void onClickBackAdd(View view) {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Метод обработки события клик по кнопке "Добавить значение".
     * @param view - компонент на который кликнули.
     */
    public void onClickAddAdd(View view) {
        EditText edit = (EditText) findViewById(R.id.addValue);
        int value = 0;
        try {
            value = Integer.parseInt(edit.getText().toString());
            if (value <= 0 || value > 20) throw new Exception("");
        } catch (Exception exp) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.str_message_title))
                    .setMessage(getResources().getString(R.string.str_message))
                    .setCancelable(false)
                    .setNegativeButton(getResources().getString(R.string.str_ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            return;
        }

        TreeActivity.avl.add(value);
        Intent intent = new Intent(this, TreeActivity.class);
        startActivity(intent);
        finish();
    }
}
