package model;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.SortedSet;

/**
 * Класс АВЛ дерева.
 * @param <E> - тип элементов, которые хранятся в вершинах дерева.
 */
public class AVLSet<E> extends AbstractSet<E>{
    /**
     * Получение итератора.
     * @return итератор, с помощью которого можно обойти дерево.
     */
    @Override
    public Iterator<E> iterator() {
        Node<E> i = root;
        while (i != null && i.left != null) i = i.left;
        return new AVLIterator(i);
    }

    /**
     * Получение количества элементов в дереве.
     * @return количество элементов в дереве.
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Проверка дерева на пустоту.
     * @return true, если дерево пустое, иначе false.
     */
    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    /**
     * Поиск элемента в дереве.
     * @param o - элемент, который ищем.
     * @return true, если элемент находится в дереве, иначе false.
     */
    @Override
    public boolean contains(Object o) {
        if (o == null) throw new NullPointerException();

        Node<E> it = getNode((E) o);
        return (it != null);
    }

    /**
     * Добавление элемента в дерево.
     * @param e - элемент, который добавляем.
     * @return true, если после выполнения операции дерево изменилось, иначе false.
     */
    @Override
    public boolean add(E e) {
        if (root == null) {
            root = new Node<E>(e);
            ++size;
            return true;
        } else {
            int cmp = 0;
            Node<E> prev = null;
            Node<E> it = root;

            if (comparator == null) {
                if (e == null) throw  new NullPointerException();

                Comparable<? super E> ec = (Comparable<? super E>) e;
                while (it != null) {
                    prev = it;
                    cmp = ec.compareTo(it.value);
                    if (cmp < 0) it = it.left;
                    else if (cmp > 0) it = it.right;
                    else break;
                }
            } else {
                while (it != null) {
                    prev = it;
                    cmp = comparator.compare(e, it.value);
                    if (cmp < 0) it = it.left;
                    else if (cmp > 0) it = it.right;
                    else break;
                }
            }

            if (cmp < 0) prev.left = new Node(e, prev);
            else if (cmp > 0) prev.right = new Node(e, prev);
            if (cmp != 0) {
                balance(prev);
                ++size;
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Удаление элемента из дерева.
     * @param o - элемент, который нужно удалить.
     * @return true, если после выполнения операции дерево изменилось, иначе false.
     */
    @Override
    public boolean remove(Object o) {
        if (o == null) throw new NullPointerException();

        Node<E> it = getNode((E) o);
        if (it != null) {
            rem(it);
            return true;
        } else return false;
    }

    /**
     * Добавление нескольких элементов.
     * @param c - коллекция, элементы которой нужно добавить в дерево.
     * @return true, если после выполнения операции дерево изменилось, иначе false.
     */
    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean res = false;
        for (E e : c) {
            res |= add(e);
        }
        return res;
    }

    /**
     * Очистка дерева.
     */
    @Override
    public void clear() {
        size = 0;
        root = null;
    }

    /**
     * Преобразование дерева к строке.
     * @return строковое представление дерева.
     */
    @Override
    public String toString() {
        String str = "[";
        boolean flag = false;
        for (E e : this) {
            if (flag) str += ", ";
            str += e;
            flag = true;
        }
        return str + "]";
    }

    /**
     * Получение вершины, в которой хранится заданный элемент.
     * @param e - элемент.
     * @return null, если элемента нет в дереве,
     *         иначе вершину, в которой хранится заданный элемент.
     */
    Node<E> getNode(E e) {
        if (e == null) throw new NullPointerException();

        if (root == null) return null;
        else {
            int cmp = 0;
            Node<E> it = root;

            if (comparator == null) {
                Comparable<? super E> ec = (Comparable<? super E>) e;
                while (it != null) {
                    cmp = ec.compareTo(it.value);
                    if (cmp < 0) it = it.left;
                    else if (cmp > 0) it = it.right;
                    else break;
                }
            } else {
                while (it != null) {
                    cmp = comparator.compare(e, it.value);
                    if (cmp < 0) it = it.left;
                    else if (cmp > 0) it = it.right;
                    else break;
                }
            }

            return  it;
        }
    }

    /**
     * Получение элемента, который хранится в корне дерева.
     * @return  null, если дерево пустое,
     *          иначе элемент, который хранится в корне дерева.
     *
     */
    public E getValueRoot() {
        if (root == null) return null;
        else return root.value;
    }

    /**
     * Получение элемента, который хранится в левом сыне заданного элемента.
     * @param e - элемент.
     * @return  null, если нет левого сына,
     *          иначе элемент, который хранится в левом сыне элемента e.
     */
    public E getValueLeft(E e) {
        Node<E> n = getNode(e);
        if (n.left == null) return null;
        else return n.left.value;
    }

    /**
     * Получение элемента, который хранится в правом сыне заданного элемента.
     * @param e - элемент.
     * @return  null, если нет правого сына,
     *          иначе элемент, который хранится в правом сыне элемента e.
     */
    public E getValueRight(E e) {
        Node<E> n = getNode(e);
        if (n.right == null) return null;
        else return n.right.value;
    }

    /**
     * Конструктор.
     */
    public AVLSet() {
        size = 0;
        root = null;
        comparator = null;
    }

    /**
     * Конструктор.
     * @param comparator - компоратор для сравнения элементов дерева.
     */
    public AVLSet(Comparator<? super E> comparator) {
        size = 0;
        root = null;
        this.comparator = comparator;
    }

    /**
     * Конструктор.
     * @param c - коллекция, элементы которой нужно добавить в дерево.
     */
    public AVLSet(Collection<? extends E> c) {
        this();
        addAll(c);
    }

    /**
     * Конструктор.
     * @param s - коллекция, элементы которой нужно добавить в дерево,
     *          а так же нужно использовать компаратор этой коллекции.
     */
    public AVLSet(SortedSet<E> s) {
        this(s.comparator());
        addAll(s);
    }

    /**
     * Конструктор.
     * @param avl - АВЛ-дерево, элементы которого нужно добавить в дерево,
     *          а так же нужно использовать компаратор этого дерева.
     */
    public AVLSet(AVLSet<E> avl) {
        this(avl.comparator());
        addAll(avl);
    }

    /**
     * Получение компаратора.
     * @return компаратор для сравнения элементов коллекции.
     */
    public Comparator<? super E> comparator() {
        return comparator;
    }

    /**
     * Размер дерева.
     */
    int size;
    /**
     * Корень дерева.
     */
    Node<E> root;
    /**
     * Компаратор для сравнения элементов коллекции.
     */
    final Comparator<? super E> comparator;

    /**
     * Класс вершины дерева.
     * @param <E> - тип элемента, который хранится в дереве.
     */
    final class Node<E> {
        /**
         * Значение элемента, который хранится в данной вершине.
         */
        E value;
        /**
         * Высота.
         */
        int h;
        /**
         * Родитель.
         */
        Node<E> parent;
        /**
         * Левый сын.
         */
        Node<E> left;
        /**
         * Правый сын.
         */
        Node<E> right;

        /**
         * Конструктор.
         * @param value - значение элемента.
         * @param parent - родитель.
         * @param left - левый сын.
         * @param right - правый сын.
         */
        Node(E value, Node<E> parent, Node<E> left, Node<E> right) {
            this.value = value;
            this.h = 1;
            this.parent = parent;
            this.left = left;
            this.right = right;
        }

        /**
         * Конструктор.
         * @param value - значение элемента.
         * @param parent - родитель.
         */
        Node(E value, Node<E> parent) {
            this(value, parent, null, null);
        }

        /**
         * Конструктор.
         * @param value - значение элемента.
         */
        Node(E value) {
            this(value, null, null, null);
        }

        /**
         * Получение коэффициента сбалансированности.
         * @return разность между высотой левого сына и высотой правого сына.
         */
        int getDif() {
            int l = (left != null) ? left.h : 0;
            int r = (right != null) ? right.h : 0;
            return l - r;
        }

        /**
         * Обновление высоты.
         */
        void updateHeight() {
            int l = (left != null) ? left.h : 0;
            int r = (right != null) ? right.h : 0;
            h = Math.max(l, r) + 1;
        }

        /**
         * Задать левого сына.
         * @param l - новый левый сын.
         */
        void setLeft(Node<E> l) {
            left = l;
            if (left != null) left.parent = this;
        }

        /**
         * Задать правого сына.
         * @param r - новый правый сын.
         */
        void setRight(Node<E> r) {
            right = r;
            if (right != null) right.parent = this;
        }
    }

    /**
     * Балансировка.
     * @param e - вершина, от которой нужно начать балансировку.
     */
    void balance(Node<E> e) {
        while (e != null) {
            e.updateHeight();
            int dif = e.getDif();
            if (dif == -2) {
                if (e.right.getDif() <= 0) leftRotation(e);
                else bigLeftRotation(e);
            } else if (dif == 2) {
                if (e.left.getDif() >= 0) rightRotation(e);
                else bigRightRotation(e);
            }
            e = e.parent;
        }
    }

    /**
     * Удаление вершины.
     * @param e - вершина, которую нужно удалить.
     */
    void rem(Node<E> e) {
        if (e.right != null) {
            Node<E> it = e.right;
            if (it.left != null) {
                while (it.left != null) it = it.left;
                e.value = it.value;
                rem(it);
            } else {
                e.value = it.value;
                e.setRight(it.right);
                --size;
            }
        } else if (e.left != null) {
            e.value = e.left.value;
            e.setRight(e.left.right);
            e.setLeft(e.left.left);
            --size;
        } else {
            if (e.parent == null) root = null;
            else if (e.parent.left == e) e.parent.left = null;
            else e.parent.right = null;
            --size;
        }
        balance(e);
    }

    /**
     * Левый поворот.
     * @param a - вершина относительно которой происходит поворот.
     */
    void leftRotation(Node<E> a) {
        Node<E> b = a.right;

        if (a.parent == null) root = b;
        else if (a.parent.left == a) a.parent.left = b;
        else a.parent.right = b;
        b.parent = a.parent;

        a.setRight(b.left);
        b.setLeft(a);

        a.updateHeight();
        b.updateHeight();
    }

    /**
     * Правый поворот.
     * @param a - вершина относительно которой происходит поворот.
     */
    void rightRotation(Node<E> a) {
        Node<E> b = a.left;

        if (a.parent == null) root = b;
        else if (a.parent.left == a) a.parent.left = b;
        else a.parent.right = b;
        b.parent = a.parent;

        a.setLeft(b.right);
        b.setRight(a);

        a.updateHeight();
        b.updateHeight();
    }

    /**
     * Большой левый поворот.
     * @param a - вершина относительно которой происходит поворот.
     */
    void bigLeftRotation(Node<E> a) {
        rightRotation(a.right);
        leftRotation(a);
    }

    /**
     * Большой правый поворот.
     * @param a - вершина относительно которой происходит поворот.
     */
    void bigRightRotation(Node<E> a) {
        leftRotation(a.left);
        rightRotation(a);
    }


    /**
     * Класс итератора.
     */
    class AVLIterator implements Iterator<E> {
        /**
         * Текущая вершина.
         */
        Node<E> it;
        /**
         * Предыдущая вершна.
         */
        Node<E> lastReturned;

        /**
         * Конструктор.
         * @param start - вершина с которой нужно начать итерирование.
         */
        AVLIterator(Node<E> start) {
            it = start;
            lastReturned = null;
        }

        /**
         * Проверка на существование следующего значения.
         * @return true, если есть следующие значение, иначе false.
         */
        @Override
        public boolean hasNext() {
            return it != null;
        }

        /**
         * Получение следующего значения.
         * @return значение.
         */
        @Override
        public E next() {
            if (it == null) throw new NoSuchElementException();

            lastReturned = it;
            it = getNext(it);

            return lastReturned.value;
        }

        /**
         * Удаления из АВЛ-дерева последнего возвращенного значения.
         */
        @Override
        public void remove() {
            if (lastReturned == null) throw new IllegalStateException();
            rem(lastReturned);
            lastReturned = null;
        }

        /**
         * Получение следующей верщшины.
         * @param i - вершина для которой нужно получить следующую вершину.
         * @return null, если больше вершин не осталось,
         *         иначе следующая вершина.
         *
         */
        Node<E> getNext(Node<E> i) {
            if (i.right != null) {
                i = i.right;
                while (i.left != null) i = i.left;
            } else {
                while (i.parent != null && i.parent.right == i) i = i.parent;
                i = i.parent;
            }
            return i;
        }
    }
}
