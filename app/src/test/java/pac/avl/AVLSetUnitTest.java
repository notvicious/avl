package pac.avl;

import org.junit.Before;
import org.junit.Test;

import java.util.Random;
import java.util.TreeSet;

import model.AVLSet;

import static org.junit.Assert.*;

/**
 * Класс для UNIT тестирования класса АВЛ дерева.
 */
public class AVLSetUnitTest {
    /**
     * Максимальное случайное число.
     */
    final static int MAX_INT = (int) 1e3;
    /**
     * Количество операция с АВЛ-деревом для каждого теста.
     */
    final static int NUM_OP = (int) 1e4;
    /**
     * Число вызовов метода clear для тестирование этого метода.
     */
    final static int NUM_CLEAR = 10;
    /**
     * Число вызовов метода toString для тестирования этого метода.
     */
    final static int NUM_OP_TO_STRING = 10;

    /**
     * АВЛ-дерево.
     */
    AVLSet<Integer> avl;
    /**
     * Сбалансированное дерево из стандартной библиотеки,
     * для проверки корректности операций с АВЛ-деревом.
     */
    TreeSet<Integer> tree;

    /**
     * Объект для генерации случайных чисел.
     */
    Random rnd;

    /**
     * Конструктор.
     */
    public AVLSetUnitTest() {
        rnd = new Random();
    }

    /**
     * Инициализация деревьев перед запуском теста.
     */
    @Before
    public void init() {
        avl = new AVLSet<>();
        tree = new TreeSet<>();
    }

    /**
     * Тестирование метода add.
     */
    @Test
    public void addTest() {
        runAdd(NUM_OP);
    }

    /**
     * Тестирование метода remove.
     */
    @Test
    public void removeTest() {
        runAdd(NUM_OP / 2);
        runRemove(NUM_OP / 2);
    }

    /**
     * Тестирование метода contains.
     */
    @Test
    public void containsTest() {
        for (int i = 0; i < NUM_OP; ++i) {
            opAddOrRemove(rnd.nextInt());
            runContains();
            opSize();
        }
    }

    /**
     * Тестирование метода clear.
     */
    @Test
    public void clearTest() {
        for (int i = 0; i < NUM_CLEAR; ++i) {
            for (int j = 0; j < NUM_OP / NUM_CLEAR; ++j) {
                opAddOrRemove(rnd.nextInt());
            }
            opClear();
            opSize();
            runContains();
        }
    }

    /**
     * Тестирование метода toString.
     */
    @Test
    public void toStringTest() {
        int param = NUM_OP / NUM_OP_TO_STRING;
        for (int i = 0; i < NUM_OP; ++i) {
            opAddOrRemove(rnd.nextInt());
            if (i % param == 0) assertEquals(avl.toString(), tree.toString());
        }
    }

    /**
     * Генерация случайного числа.
     * @return целое случайное число из отрезка [0, MAX_INT].
     */
    int nextInt() {
        return rnd.nextInt(MAX_INT);
    }

    /**
     * Выполнение операции add заданное число раз.
     * @param count - количество раз, которое нужно выполнить операцию add.
     */
    void runAdd(int count) {
        for (int i = 0; i < count; ++i) opAdd(nextInt());
    }

    /**
     * Выполнение операции remove заданное число раз.
     * @param count - количество раз, которое нужно выполнить операцию remove.
     */
    void runRemove(int count) {
        for (int i = 0; i < count; ++i) opRemove(nextInt());
    }

    /**
     * Выполнение операции contains для всех целых чисел из отрезка [0, MAX_INT].
     */
    void runContains() {
        for (int i = 0; i <= MAX_INT; ++i) opContains(i);
    }

    /**
     * Выполнение операции add.
     * @param val - значение, которое добавляется в дерево.
     */
    void opAdd(int val) {
        assertEquals(avl.add(val), tree.add(val));
    }

    /**
     * Выполнение операции remove.
     * @param val - значение, которое удаляется из дерева.
     */
    void opRemove(int val) {
        assertEquals(avl.remove(val), tree.remove(val));
    }

    /**
     * Выполнение операции contains.
     * @param val - значение, которое нужно найти в дереве.
     */
    void opContains(int val) {
        assertEquals(avl.contains(val), tree.contains(val));
    }

    /**
     * Выполнение операции add или remove (выбор случаен).
     * @param val - значение, которое добавляется/удаляется.
     */
    void opAddOrRemove(int val) {
        if (rnd.nextBoolean()) opAdd(val);
        else opRemove(val);
    }

    /**
     * Выполнение операции clear.
     */
    void opClear() {
        avl.clear();
        tree.clear();
    }

    /**
     * Выполнение операции size.
     */
    void opSize() {
        assertEquals(avl.size(), tree.size());
    }
}